## What you need

You will need 
[npm](https://www.npmjs.com/)
to run the app.


## How to run the app ?

### Step one :  Clone the Git repository

With HTTPS : `https://gitlab.com/ThomasPetitYnov/socialnetwork-frontend.git`

With SSH : `git@gitlab.com:ThomasPetitYnov/socialnetwork-frontend.git`

### Step two :  Go to the folder

`cd socialnetwork-frontend`

### Step three :  Install all packages

`npm install`

### Step four :  Run the server

`npm run start` 

### Step five :  Install the back-end part

**Of course, don't process this step if it's already done !**

The front will work but with no database and backend... it won't work. 
To fix this issue, make the back work with the same process, explained on 
[its own repository](https://gitlab.com/ThomasPetitYnov/socialnetwork-backend).


---------


## How to deal with the app ?

### Step one :  Create a user

When the front server is launched, the front app is opened is the default browser. 
Click `REGISTER` button to reach the Sign-In page.

### Step two :  Log in the user

After user's creation, you are redirected to Login page, use your identifiers to log in.

### Step three :  ENJOY

On each page, you can
* Navigate to Home page with the logo ou house icon on top-left
* Navigate to Profile page with the Account icon on top-right
* Navigate to Chat page with the chat icon on top-right
* Disconnect with `LOGOUT` button on top-right

On the home page, you can
* Post something with the form on the top of the page
* Like posts with the `LIKE` button
* Delete your posts with the `DELETE` button
* Comment posts with the `COMMENT` button
* Navigate to other users' profile pages by clicking their names (in posts or comments)

On your profile page, you can
* Update your profile photo by clicking the `CHANGE PHOTO` button. A window will be 
opened with an upload field. Select a photo on your computer, and you will be asked 
to crop the photo. When you are satisfied, you can click the `UPLOAD` button
* Watch and handle all your posts

On another user's profile page, you can
* Ask user to become your friend
    * `ASK FRIEND` when you want to ask
    * `ACCEPT` or `REFUSE` to accept or refuse user's demand
    * `REMOVE FRIEND` to remove this user from your friends
* Watch each posts of selected user (and related comments)

On chat page, you can
* Select users to individually chat with them
