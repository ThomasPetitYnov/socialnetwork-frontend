import React, { Component } from 'react';
import './Chat.css';
import { withStyles } from '@material-ui/core/styles';
import { redirectToLogin } from '../helpers/Redirections';
import CombineStyles from '../helpers/CombineStyles';
import GlobalStyle from '../helpers/GlobalStyle';
import AppBarCustom from "../helpers/AppBarCustom";
import axios from "axios";
import io from "socket.io-client";
import Paper from "@material-ui/core/Paper";
import classNames from "classnames";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

const LocalStyle = theme => ({
    paperLocalChat: {
        marginTop: '10px',
        padding: theme.spacing.unit,
        height: 'calc(100vh - 116px)',
    },
    paperLocalUsers: {
        marginTop: '10px',
        padding: 0,
        height: 'calc(100vh - 100px)',
    },
    textField: {
        width: '100%',
    },
    messages: {
        height: 'calc(100vh - 300px)',
        overflowY: 'auto',
        whiteSpace: 'pre-wrap'
    },
    selfMessage: {
        backgroundColor: '#d1ffae',
        padding: '10px',
        borderRadius: '15px',
        width: '70%',
        marginLeft: 'calc(30% - 20px)',
        marginTop: '5px',
    },
    otherMessage: {
        backgroundColor: '#a0e1ff',
        padding: '10px',
        borderRadius: '15px',
        width: '70%',
        marginTop: '5px',
    },
    messageContent: {
        display: 'flex',
    },
    messageHour: {
        fontSize: '75%',
        color: 'grey',
        textAlign: 'right',
    },
    marginLeftCustom: {
        marginLeft: '8px',
    },
    userList: {
        backgroundColor: '#ececec',
        borderBottom: '1px solid #c8c8c8',
        padding: '5px 5px 5px 10px',
        height: '30px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'left',
        flexDirection: 'row',
        fontSize: '80%',
        '&:hover': {
            backgroundColor: '#c8c8c8',
            cursor: 'pointer',
        },
    },
    photo: {
        borderRadius: '15px',
    },
    userSelected: {
        backgroundColor: '#ececec',
        padding: '5px 5px 5px 10px',
        height: '40px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'left',
        flexDirection: 'row',
        borderRadius: '15px',
        marginBottom: '10px',
    },
});

class Chat extends Component {

    state = {
        user: [],
        users: [],
        token: sessionStorage.getItem('token'),
        message: '',
        messages: [],
        selectedUser: [],
        active: -1,
        interval: null,
        previousRoom: null,
        currentRoom: null,
        firstLoading: 0,
        phoneSize: window.innerWidth <= 600,
        showUsersMobile: true,
        showChatMobile: false,
        socket: io('localhost:8080'),
    };

    constructor(props) {
        super(props);
        if (!this.state.token) {
            redirectToLogin(this.props);
        }

        this.state.socket.on('RECEIVE_MESSAGE', function(data){
            addMessage(data);
        });

        const addMessage = data => {
            this.setState({messages: [...this.state.messages, data]});
            this.scrollBottom();
        };
    }

    sendMessage = () => {
        this.state.socket.emit('SEND_MESSAGE', {
            authorId: this.state.user._id,
            recipientId: this.state.selectedUser._id,
            date: Date.now(),
            author: this.state.user.firstName + ' ' + this.state.user.lastName,
            recipient: this.state.selectedUser.firstName + ' ' + this.state.selectedUser.lastName,
            message: this.state.message,
            room: this.state.currentRoom._id,
        });
        this.setState({message: ''});
    };

    changeCurrentRoom = () => {
        this.state.socket.emit('CHANGE_ROOM', this.state.currentRoom._id);
    };

    scrollBottom = () => {
        let component = this;
        let objDiv = document.getElementById("messages");
        if (!objDiv) return;
        if (this.state.firstLoading === 1) {
            setTimeout( function () {
                objDiv.scrollTop = objDiv.scrollHeight;
                component.setState({firstLoading: 0});
            }, 1000);
        }
        if (((objDiv.scrollHeight - 500) - objDiv.scrollTop) < 100) {
            objDiv.scrollTop = objDiv.scrollHeight;
        }
    };

    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions.bind(this));
        this.getCurrentUser();
        this.getUsers();
    }

    updateDimensions() {
        this.setState({phoneSize: window.innerWidth <= 600})
    }

    getCurrentUser = () => {
        axios.get("http://localhost:3001/users/" + this.state.token)
            .then(data => {
                    this.setState({user: data['data'], username: data['data'].firstName + ' ' + data['data'].lastName});
                }
            );
    };

    getUsers = () => {
        axios.get("http://localhost:3001/users")
            .then(data => {
                    let firstUser = null;
                    let active = -1;
                    if (data['data'][0]._id !== this.state.user._id) {
                        firstUser = data['data'][0];
                        active = 0;
                    } else {
                        firstUser = data['data'][1];
                        active = 1;
                    }
                    this.chooseUser(firstUser._id);
                    this.setState({users: data['data'], selectedUser: firstUser, active: active});
                }
            );
    };

    chooseUser = async userId => {
        this.setState({showUsersMobile: false, showChatMobile: true});
        await axios.get("http://localhost:3001/users/" + userId)
            .then(data => {
                    this.setState({selectedUser: data['data'], firstLoading: 1});
                    this.changeRoom(userId);
                    this.getMessages(userId);
                }
            );
    };

    changeRoom = async userId => {
        await axios.get("http://localhost:3001/room/" + userId + "/" + this.state.user._id)
            .then(data => {
                    this.setState({previousRoom: this.state.currentRoom});
                    this.setState({currentRoom: data['data'][0]});
                    this.changeCurrentRoom();
                }
            );
    };

    getMessages = userId => {
        axios.get("http://localhost:3001/messages/" + userId + "/" + this.state.user._id)
            .then(data => {
                    this.setState({messages: data['data']});
                    this.scrollBottom();
                }
            );
    };

    backToUsers = () => {
        this.setState({showUsersMobile: true, showChatMobile: false});
    };

    formatDate = date => {
        let d = new Date(date);
        let day = d.getDay() < 10 ? '0' + d.getDay() : d.getDay();
        let month = d.getMonth() < 10 ? '0' + d.getMonth() : d.getMonth();
        return `${day}/${month}/${d.getFullYear()}`;
    };
    formatHour = date => {
        let d = new Date(date);
        let minutes = d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes();
        return `${d.getHours()}:${minutes}`;
    };

    render() {
        const currentUser = this.state.user;
        const users = this.state.users;
        const { classes } = this.props;
        return (
            <div className="Home">
                <AppBarCustom data={this.props} />

                <div>
                    <div>
                        <div>
                            {
                                this.state.phoneSize &&
                                <Grid container justify={"center"} style={{maxWidth: '100%'}}>
                                    {
                                        this.state.showUsersMobile &&
                                        <Grid item xs={12} sm={3}>
                                            <Paper className={classNames(classes.paperGlobal, classes.paperLocalUsers)}
                                                   elevation={10}>
                                                {users.map((user, index) => (
                                                    user._id !== currentUser._id &&
                                                    <div key={user._id}
                                                         id={index}
                                                         className={classes.userList}
                                                         onClick={() => this.chooseUser(user._id)}>
                                                        <img src={"http://localhost:3001/images/profile/" + user.photo}
                                                             className={classes.photo}
                                                             alt={user.firstName}
                                                             width="20"
                                                             height="20"
                                                        />
                                                        <span className={classes.marginLeftCustom}>
                                                            <span>{user.firstName} </span>
                                                            <span>{user.lastName} </span>
                                                        </span>
                                                    </div>
                                                ))}
                                            </Paper>
                                        </Grid>
                                    }
                                    {
                                        this.state.showChatMobile &&
                                        <Grid item xs={12} sm={8}>
                                            <Paper className={classNames(classes.paperGlobal, classes.paperLocalChat)}
                                                   elevation={10}>
                                                <div className={classes.selectedUser}>
                                                    {
                                                        this.state.selectedUser &&
                                                        <div className={classes.userSelected}>
                                                            {
                                                                this.state.phoneSize &&
                                                                <div
                                                                    style={{
                                                                        fontSize: '300%',
                                                                        marginRight: '10px',
                                                                        marginBottom: '13px',
                                                                        cursor: 'pointer',
                                                                    }}
                                                                    onClick={() => this.backToUsers()}
                                                                >&#8249;</div>
                                                            }
                                                            <img
                                                                src={"http://localhost:3001/images/profile/" + this.state.selectedUser.photo}
                                                                className={classes.photo}
                                                                alt={this.state.selectedUser.firstName}
                                                                width="30"
                                                                height="30"
                                                            />
                                                            <span className={classes.marginLeftCustom}>
                                                                <span>{this.state.selectedUser.firstName} </span>
                                                                <span>{this.state.selectedUser.lastName} </span>
                                                            </span>
                                                        </div>
                                                    }
                                                </div>
                                                <div id="messages" className={classes.messages}>
                                                    {this.state.messages.map(message => {
                                                        return (
                                                            <div key={message._id} className={
                                                                message.authorId === currentUser._id ?
                                                                    classes.selfMessage :
                                                                    classes.otherMessage}
                                                            >
                                                                {message.message}

                                                                <div className={classes.messageHour}>
                                                                    {message.author} - {this.formatDate(message.date)} - {this.formatHour(message.date)}
                                                                </div>
                                                            </div>

                                                        )
                                                    })}
                                                </div>
                                                <div>
                                                    <hr/>
                                                    <Grid container>
                                                        <Grid item xs={10} md={11}>
                                                            <TextField
                                                                placeholder="Message"
                                                                multiline={true}
                                                                rows={5}
                                                                className={classes.textField}
                                                                value={this.state.message}
                                                                onChange={ev => this.setState({message: ev.target.value})}
                                                            />
                                                        </Grid>
                                                        <Grid item xs={2} md={1}>
                                                            <Button size="small"
                                                                    color="primary"
                                                                    className={classNames(classes.controlButton, classes.commentButton)}
                                                                    onClick={() => this.sendMessage()}
                                                            >
                                                                <i className="material-icons">send</i>
                                                            </Button>
                                                        </Grid>
                                                    </Grid>
                                                </div>
                                            </Paper>
                                        </Grid>
                                    }
                                </Grid>
                            }
                            {
                                !this.state.phoneSize &&
                                <Grid container justify={"center"} style={{maxWidth: '100%'}}>
                                    <Grid item xs={12} sm={3}>
                                        <Paper className={classNames(classes.paperGlobal, classes.paperLocalUsers)} elevation={10}>
                                            {users.map((user, index) => (
                                                user._id !== currentUser._id &&
                                                <div key={user._id}
                                                     id={index}
                                                     className={classes.userList}
                                                     onClick={() => this.chooseUser(user._id)}>
                                                    <img src={"http://localhost:3001/images/profile/" + user.photo}
                                                         className={classes.photo}
                                                         alt={user.firstName}
                                                         width="20"
                                                         height="20"
                                                    />
                                                    <span className={classes.marginLeftCustom}>
                                                    <span>{user.firstName} </span>
                                                    <span>{user.lastName} </span>
                                                </span>
                                                </div>
                                            ))}
                                        </Paper>
                                    </Grid>
                                    <Grid item xs={12} sm={8}>
                                        <Paper className={classNames(classes.paperGlobal, classes.paperLocalChat)} elevation={10}>
                                            <div className={classes.selectedUser}>
                                                {
                                                    this.state.selectedUser &&
                                                    <div className={classes.userSelected}>
                                                        <img src={"http://localhost:3001/images/profile/" + this.state.selectedUser.photo}
                                                             className={classes.photo}
                                                             alt={this.state.selectedUser.firstName}
                                                             width="30"
                                                             height="30"
                                                        />
                                                        <span className={classes.marginLeftCustom}>
                                                    <span>{this.state.selectedUser.firstName} </span>
                                                    <span>{this.state.selectedUser.lastName} </span>
                                                </span>
                                                    </div>
                                                }
                                            </div>
                                            <div id="messages" className={classes.messages}>
                                                {this.state.messages.map(message => {
                                                    return (
                                                        <div key={message._id} className={
                                                            message.authorId === currentUser._id ?
                                                                classes.selfMessage :
                                                                classes.otherMessage}
                                                        >
                                                            {message.message}

                                                            <div className={classes.messageHour}>
                                                                {message.author} - {this.formatDate(message.date)} - {this.formatHour(message.date)}
                                                            </div>
                                                        </div>

                                                    )
                                                })}
                                            </div>
                                            <div>
                                                <hr/>
                                                <Grid container>
                                                    <Grid item xs={10} md={11}>
                                                        <TextField
                                                            placeholder="Message"
                                                            multiline={true}
                                                            rows={5}
                                                            className={classes.textField}
                                                            value={this.state.message}
                                                            onChange={ev => this.setState({message: ev.target.value})}
                                                        />
                                                    </Grid>
                                                    <Grid item xs={2} md={1}>
                                                        <Button size="small"
                                                                color="primary"
                                                                className={classNames(classes.controlButton, classes.commentButton)}
                                                                onClick={() => this.sendMessage()}
                                                        >
                                                            <i className="material-icons">send</i>
                                                        </Button>
                                                    </Grid>
                                                </Grid>
                                            </div>
                                        </Paper>
                                    </Grid>
                                </Grid>
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const combinedStyles = CombineStyles(GlobalStyle, LocalStyle);

export default withStyles(combinedStyles)(Chat);
