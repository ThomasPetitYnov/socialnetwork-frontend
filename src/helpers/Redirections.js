export const redirectToLogin = (props) =>  {
    props.history.push('/login');
};

export const redirectToProfile = (props) =>  {
    props.history.push('/profile');
};

export const redirectToHome = (props) =>  {
    props.history.push('/');
};

export const redirectToUser = (props, userId) =>  {
    props.history.push('/user/' + userId);
};